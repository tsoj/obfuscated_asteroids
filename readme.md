#### Controls
- Movement:  
`w`, `a`, `s`, `d`
- Fire:  
`space`
- Quit:  
`q`
- Pause:  
`p`, then use any other key to resume

```
   /\
  //\\
 /    \
/______\
  /\/\
```
#### Compile and run
###### Linux
```
cc -std=c99 -O3 prog.c -o prog
./prog
```

###### Windows
The only way I found to make it work is to use [MSYS2](https://www.msys2.org). When using the MSYS2 shell you can install gcc via pacman.
```
cc -std=c99 -O3 prog.c -o prog
./prog.exe
```
#### Warnings
- `-Wno-missing-prototypes`
- `-Wno-shorten-64-to-32`
- `-Wno-sign-conversion`
- `-Wno-implicit-int-float-conversion`
- `-Wno-missing-field-initializers`
- `-Wno-implicit-float-conversion`
- `-Wno-vla`
- `-Wno-float-conversion`
- `-Wno-comma`
- `-Wno-shadow`
- `-Wno-format`
- `-Wno-empty-body`
- `-Wno-misleading-indentation`
- `-Wno-maybe-uninitialized`
- You may experience a mild [motion aftereffect](https://en.wikipedia.org/wiki/Motion_aftereffect) when playing for too long
#### Highscore
Score will be shown in the top left corner.  
My best was **178**. If you beat that I'll send you the image of a very expensive chocolate.
#### Features
- Procedural star generation
- Very space-efficient for-loops
- No use of `!=` or `==`
- Smooth rendering even on slow terminals
- Advanced ascii-art
- Cutting edge physics engine and collision detection
- Probably not many segfaults
- Not a virus
- Runs on my machine
- Every letter in the English alphabet has a variable or function named after it
#### Tips
- Adjust your terminal size for different experiences
- Run this program in a virtual console to immerse yourself in the game
