WARNINGS=-Wall -Wextra -pedantic

DISABLED_WARNINGS=-Wno-missing-prototypes -Wno-shorten-64-to-32 -Wno-sign-conversion -Wno-implicit-int-float-conversion -Wno-missing-field-initializers -Wno-implicit-float-conversion -Wno-vla -Wno-float-conversion -Wno-comma -Wno-shadow -Wno-format -Wno-empty-body -Wno-misleading-indentation -Wno-maybe-uninitialized

prog: prog.c
	${CC} -std=c99 -O3 $(WARNINGS) $(DISABLED_WARNINGS) prog.c -o prog
